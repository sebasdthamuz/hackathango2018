from pyspark.sql.functions import *
from pyspark.sql.types import StringType, StructType, StructField, IntegerType, DoubleType, DateType, Row
import numpy as np
from pyspark.sql import Row
from datetime import datetime
from functools import reduce
import csv

HCK_PERSONA_FINAL = spark.read.format("csv").option("header", "true").load("./datasets/HCK_PERSONA_FINAL.csv")
HCK_TRANSACCIONES_FINAL = spark.read.format("csv").option("header", "true").load("./datasets/HCK_TRANSACCIONES_FINAL.csv")
HCK_DIGITAL_FINAL = sqlContext.read.csv("./datasets/HCK_DIGITAL_FINAL.csv", header=True)
HCK_PERSONA_FINAL.select('desc_segmento').distinct().count()
HCK_DIGITAL_FINAL.fillna(0).show()
ids_totales = HCK_PERSONA_FINAL.select('id_cliente').distinct()
ids_digitales = HCK_DIGITAL_FINAL.select('id_cliente').distinct()
tarjeta_digital = HCK_DIGITAL_FINAL.filter(col("bmovil").isNull()).filter(col("alerta_bancomer").isNull()).filter(col("bcom").isNull()).select('id_cliente').distinct().count()
bmovil = HCK_DIGITAL_FINAL.filter(col("tarjeta_digital").isNull()).filter(col("alerta_bancomer").isNull()).filter(col("bcom").isNull()).select('id_cliente').distinct().count()
alerta_bancomer = HCK_DIGITAL_FINAL.filter(col("bmovil").isNull()).filter(col("tarjeta_digital").isNull()).filter(col("bcom").isNull()).select('id_cliente').distinct().count()
bcom = HCK_DIGITAL_FINAL.filter(col("bmovil").isNull()).filter(col("alerta_bancomer").isNull()).filter(col("tarjeta_digital").isNull()).select('id_cliente').distinct().count()
tarjeta_digital
bmovil
alerta_bancomer
bcom
HCK_DIGITAL_FINAL.filter(col("bmovil").isNull()).filter(col("alerta_bancomer").isNull()).filter(col("bcom").isNull()).filter(col("tarjeta_digital").isNull()).select('id_cliente').distinct().count()
ids_totales.count()
ids_digitales.count()
ids_no_digitales = ids_totales.subtract(ids_digitales)
ids_no_digitales.count()
ids_no_digitales_list = ids_no_digitales.rdd.map(lambda x: x[0]).collect()
HCK_DIGITAL_FINAL.filter(col("bmovil").isNull()).select('id_cliente').distinct().count()
nodigitalescollect = ids_no_digitales.rdd.map(lambda x: x).collect()
with open('./datasets/ids_no_digitales.csv', "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerows(nodigitalescollect)

HCK_TRANSACCIONES_FINAL.filter('transaccion_fraude = 1').show()

df1 = HCK_TRANSACCIONES_FINAL.alias('df1')
df2 = HCK_PERSONA_FINAL.alias('df2')
df3 = df1.join(df2, df1.id_tarjeta == df2.id_tarjeta).select('df2.id_cliente','df1.*')
ids_clientes_fraude = df3.filter('transaccion_fraude = 1').select('id_cliente').distinct().rdd.map(lambda x: x[0]).collect()
len(ids_clientes_fraude)
df3.filter()
#HCK_TRANSACCIONES_FINAL.alias('a').join(HCK_TRANSACCIONES_FINAL.alias('b'),col('b.id_cliente') == col('a.id_cliente'))


'C000000000046169' in ids_no_digitales_list
ids_no_digitales_list
PERSONA_FINAL = sqlContext.read.csv("./datasets/PERSONA_FINAL.csv", header=True)
PERSONA_FINAL.select('id_cliente').show()
PERSONA_FINAL = PERSONA_FINAL.withColumn('es_digital', es_digital_udf('id_cliente'))
PERSONA_FINAL = PERSONA_FINAL.withColumn('reporto_fraude', es_digital_udf('id_cliente'))
HCK_PERSONA_FINAL.show()
PERSONA_FINAL.show()
PERSONA_FINAL.orderBy(col('nominado').desc()).show()
PERSONA_FINAL.select('id_cliente').distinct().count()
PERSONA_FINAL.show()

import datetime
now = datetime.datetime.now()
PERSONA_FINAL = PERSONA_FINAL.withColumn("date", str(now)[:10])

es_digital_udf = udf(lambda x: 0 if x in ids_no_digitales_list else 1, IntegerType())
reporto_fraude_udf = udf(lambda x: 1 if x in ids_clientes_fraude else 0, IntegerType())
PERSONA_FINAL.count()
PERSONA_FINAL = PERSONA_FINAL.orderBy(col('nominado').desc()).dropDuplicates(['id_cliente'])
pf_list = PERSONA_FINAL.rdd.map(lambda x: x).collect()
pf_list.insert(0,PERSONA_FINAL.columns)
with open('./datasets/PERSONA_FINAL.csv', "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerows(pf_list)
